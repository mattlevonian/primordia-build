# README #

This repo contains all the Unity project files and folders, except the all-important Assets folder.

### How do I get set up? ###

You should not be pushing to this repo unless you know what you are doing. To work on the project, make a local copy of this repo into a folder called "scene-local" or something. Then head over to the [assets repository](https://bitbucket.org/mattlevonian/primordia-assets) and clone it into a folder on the same directory level called "masterassets" (or whatever).

Then you need to create a [directory symlink](http://www.tech-recipes.com/rx/13085/windows-7-how-to-create-symlinks-symbolic-links/) called "Assets" in the Unity project folder ("scene-local") that links the "masterassets" directory. 

While working like this, you can't make changes to project settings (they won't get saved unless you push, which you shouldn't). You should only push changes to the Assets folder, and those will be pushed to the primordia-assets repository, not this one.